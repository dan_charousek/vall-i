<?php

namespace Tests\DanCharousek\VallI;

use PHPUnit\Framework\TestCase;
use DanCharousek\VallI\{VallI, SessionNotInitializedException, FileNotFoundException, NoFormSubmittedException};

class ValliTest extends TestCase
{
	
	public function testLoadFromFileSuccess()
	{
		$form = VallI::createFromFile('examples/basic-example/basic-example-form', ['options' => []]);
		$this->assertInstanceOf(VallI::class, $form);
	}

	public function testLoadFromFileFailure()
	{
		$this->expectException(FileNotFoundException::class);
		$form = VallI::createFromFile('lorem-path');
	}
	
}
