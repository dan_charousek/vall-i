# Vall-I #

Vall-I is PHP form validation library, that allows you to quickly validate any form in 3 easy steps:

### Get started ###

* Get Vall-I
* Setup validation rules
* Validate

### How do I start? ###

Get Vall-I via composer:


```
#!bash

composer require dancharousek/vall-i
```

Do not forget to include composer's autoload file.

```
#!php

require_once 'vendor/autoload.php';
```

And import VallI:

```
#!php

use DanCharousek\VallI\VallI;
```


Create new Vall-I instance:


```
#!php
<?php

$form = VallI::createFromFile('path/to/my/form', $parameters = [], $ext = '.php');
$form->save();
```

And print form:

```
#!php

<html><body><?= $form ?></body></html>
```

Within form file, attach validation rules to form elements:

```
#!html

<form method="POST" action="submit.php">
    Username:
    <input type="text" name="username" valli-rules="required:true" valli-name="Username">
    Password:
    <input type="password" name="password" valli-rules="required:true|minlength:6" valli-name="Password">
</form>
```

Validation within submit.php file:

```
#!php

<?php

$result = VallI::validate();
if($result->isValid()) {
    echo 'Form is valid, doing post validation stuff...';
    $data = $result->getValues();
} else {
    // Form is not valid, get error messages and redirect back to form
    $errorMessages = $result->getErrorMessages(); // can be flashed as error messages or something, processing those messages is up to you
    // Redirecting back to form - inline error messages will be automatically printed
    Header('Location: index.php');
    exit;
    
}
```