<form method="POST" action="basic-example-submit.php">

	<div class="form-group">
		<label for="email">Email address</label>
		<input type="email" class="form-control" id="email" name="email" valli-rules="required:true|isemail:true" valli-name="Email address">
	</div>

	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" class="form-control" id="password" name="password" valli-rules="required:true|minlength:6" valli-name="Secret code">
	</div>

	<div class="form-group">
		<label for="select-one">Example select (select 4)</label>
		<select class="form-control" id="select-one" name="select-one" valli-rules="equals:4" valli-name="First choice">
		<?php foreach($options as $option) : ?>
			<option><?= $option ?></option>
		<?php endforeach; ?>
		</select>
	</div>

	<div class="form-group">
		<label for="select-two">Example multiple select</label>
		<select multiple class="form-control" id="select-two" name="selecttwo[]" valli-rules="required:true|contains:4,2" valli-name="Multiple select">
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
		</select>
	</div>

	<div class="form-group">
		<label for="textarea">Example textarea</label>
		<textarea class="form-control" id="textarea" rows="3" name="message" valli-rules="maxlength:50" valli-name="Message"></textarea>
	</div>

	<div class="form-check">
		<label class="form-check-label">
			<input type="checkbox" class="form-check-input" name="agreement" valli-rules="required:true" valli-name="Agreement">
			Check me out
		</label>
	</div>

	<button type="submit" class="btn btn-primary">Submit</button>
</form>