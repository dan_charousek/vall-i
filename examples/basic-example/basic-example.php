<?php

session_start();

require_once '../../vendor/autoload.php';

use DanCharousek\VallI\VallI;

$form = VallI::createFromFile('basic-example-form', [
    'options' => [1, 2, 3, 4]
]);

$form->setNotToRestore(['password', 'agreement']);
$form->save();

?>
<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <title>Vall-I basic example</title>

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>

    <body>
        <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Vall-I basic usage</h1>
                </div>
            </div>
        </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form ?>
                </div>
            </div>
        </div>
    </body>

</html>