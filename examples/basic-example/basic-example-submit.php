<?php

session_start();

require_once '../../vendor/autoload.php';

use DanCharousek\VallI\VallI;

$result = VallI::validate();

if($result->isValid()) {
    echo 'Is valid, doing post validation stuff with ';
    print_r($result->getValues());
} else {
    // Is invalid, redirecting back
    Header('Location: basic-example.php');
    exit;
}