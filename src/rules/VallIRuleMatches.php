<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleMatches extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        return preg_match($value, $data);
    }

}