<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleEquals extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        return $data == $this->value;
    }

    public function getErrorMessage()
    {
        return sprintf(VallIRule::$errorMessages[get_class($this)], $this->attachedInputName, $this->value);
    }

}