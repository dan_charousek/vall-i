<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleIsNumber extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        return is_numeric($data);
    }

    public function getErrorMessage()
    {
        return sprintf(VallIRule::$errorMessages[get_class($this)], $this->attachedInputName, $this->value);
    }

}