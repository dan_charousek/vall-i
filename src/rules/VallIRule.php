<?php

namespace DanCharousek\VallI\Rules;

class NoValliRuleException extends \Exception {}

class VallIRule
{

	protected $value;
	protected $attachedInputName;

	public static $nameToInstance = [
		'required'      => VallIRuleRequired::class,
		'minlength'     => VallIRuleMinlength::class,
		'equals'        => VallIRuleEquals::class,
		'isemail'       => VallIRuleIsEmail::class,
		'isin'          => VallIRuleIsIn::class,
		'isnumber'      => VallIRuleIsNumber::class,
		'isurl'         => VallIRuleIsUrl::class,
		'matches'       => VallIRuleMatches::class,
		'maxlength'     => VallIRuleMaxlength::class,
		'notequal'      => VallIRuleNotEqual::class,
		'contains'		=> VallIRuleContains::class
	];
 
	public static $errorMessages = [
		VallIRuleRequired::class    => '%s is required',
		VallIRuleMinlength::class   => '%s must be at least %s characters long',
		VallIRuleEquals::class      => '%s must be equal to %s',
		VallIRuleIsEmail::class     => '%s must be valid email',
		VallIRuleIsIn::class        => '%s must be in %s',
		VallIRuleIsNumber::class 	=> '%s must be number',
		VallIRuleIsUrl::class 		=> '%s must be valid url',
		VallIRuleMatches::class   	=> '%s must match pattern',
		VallIRuleMaxlength::class   => '%s must be at maximum %s characters long',
		VallIRuleNotEqual::class   	=> '%s mustn\'t be equal to %s',
		VallIRuleContains::class 	=> '%s must contains all of the following: %s'
	];

	public function __construct($value)
	{
		$this->value = $value;
	}

	public function getErrorMessage()
	{
		return sprintf(VallIRule::$errorMessages[get_class($this)], $this->attachedInputName);
	}

	public function setAttachedInputName($name)
	{
		$this->attachedInputName = $name;
	}

	public static function create($name, $value)
	{
		if(!isset(VallIRule::$nameToInstance[$name]))
			throw new NoValliRuleException('There is no such valli rule with name ' . $name);

		return new VallIRule::$nameToInstance[$name]($value);
	}

}