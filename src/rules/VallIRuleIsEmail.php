<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleIsEmail extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        return filter_var($data, FILTER_VALIDATE_EMAIL);
    }

}