<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleMaxlength extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        return strlen($data) <= $this->value;
    }

    public function getErrorMessage()
    {
        return sprintf(VallIRule::$errorMessages[get_class($this)], $this->attachedInputName, $this->value);
    }

}