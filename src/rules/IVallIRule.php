<?php

namespace DanCharousek\VallI\Rules;

interface IVallIRule
{

    function matches($data);
    function getErrorMessage();

}