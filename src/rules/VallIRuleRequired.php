<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleRequired extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        return !empty($data);
    }

}