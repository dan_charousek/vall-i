<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleContains extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        $values = explode(',', $this->value);
        $containsAllValues = !array_diff($values, $data);
        return $containsAllValues;
    }

    public function getErrorMessage()
    {
        return sprintf(VallIRule::$errorMessages[get_class($this)], $this->attachedInputName, $this->value);
    }

}