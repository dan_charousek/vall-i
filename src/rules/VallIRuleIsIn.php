<?php

namespace DanCharousek\VallI\Rules;

class VallIRuleIsEmail extends VallIRule implements IVallIRule
{

    public function __construct($value)
    {
        parent::__construct($value);
    }

    public function matches($data): bool
    {
        return in_array($data, json_decode($this->value));
    }

}