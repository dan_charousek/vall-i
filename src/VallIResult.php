<?php

namespace DanCharousek\VallI;

class VallIResult
{

    /**
     * Form wrapper
     * 
     * @var DOMElement
     */
    public $formWrapper;

    /**
     * Request date
     * 
     * @var array
     */
    private $data;

    /**
     * List of error messages
     * 
     * @var array
     */
    private $errorMessages = [];

    /**
     * Attribute used for fast validation
     * 
     * @var boolean
     */
    private $valid = true;

    /**
     * Set by VallI
     * 
     * @var array
     */
    public $inputClasses;

    /**
     * Element's not to restore
     * 
     * @var array
     */
    public $notToRestore;

    /**
     * Validates inputs
     * 
     * @return void
     */
    public function validate()
    {
        foreach($this->formWrapper->getElements() as $key => $element) {
            if($element->isValid($this->data[$key] ?? null)) {
                $element->setValue(in_array($key, $this->notToRestore) ? '' : $this->data[$key]);
                $element->setClass($this->inputClasses['valid']);
            } else {
                $element->setValue('');
                $element->setClass($this->inputClasses['invalid']);
                $this->errorMessages = array_merge($this->errorMessages, $element->getErrorMessages());
            }
        }
    }

    /**
     * Fast validation used when submitting just to know if should redirect or not
     * 
     * @return bool
     */
    public function isValidFast(): bool
    {
        foreach($this->formWrapper->getElements() as $key => $element) {
            if(!$element->isValid($this->data[$key] ?? null))
                return $this->valid = false;
        }
        return true;
    }

    public function __construct(VallIForm $form, array $data)
    {
        $this->formWrapper = $form;
        $this->data = $data;
    }


    /**
     * Returns form wrapper
     * 
     * @return VallIForm
     */
    public function getFormWrapper(): VallIForm
    {
        return $this->formWrapper;
    }

    /**
     * Checks if whole request is valid
     * 
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid && empty($this->errorMessages);
    }

    /**
     * Returns data from request
     * 
     * @return array
     */
    public function getValues()
    {
        return $this->data;
    }

    /**
     * Returns error messages
     * 
     * @return array
     */
    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }

}