<?php

namespace DanCharousek\VallI;

class VallIForm
{

    const INLINE_MESSAGES_CLASSNAME = 'valli-inline-error-messages-wrapper';

    /**
     * @var DOMDocument
     */
    private $domDocument;

    /**
     * Form dom
     * 
     * @var DOMElement
     */
    private $formDom;

    /**
     * List of valli elements
     * 
     * @var array
     */
    private $valliElements = [];

    public function __construct(string $source)
    {
		$dom = new \DOMDocument();

		libxml_use_internal_errors(true);
		    $dom->loadHTML(mb_convert_encoding($source, 'HTML-ENTITIES', 'UTF-8'));
		libxml_clear_errors();

        $this->domDocument = $dom;
        $this->formDom = $dom->getElementsByTagName('form')->item(0);

        foreach($this->domDocument->getElementsByTagName('*') as $element) {
            if(empty($element->getAttribute('valli-rules')))
                continue;

            $newElement = Elements\VallIElement::create($this->domDocument, $element);    
            $this->valliElements[str_replace('[]', '', $element->getAttribute('name'))] = $newElement;

            /**
             * Adding inline error messages wrapper after element
             */
            $wrapper = $dom->createElement('div');
            $wrapper->setAttribute('class', VallIForm::INLINE_MESSAGES_CLASSNAME . ' ' . VallIForm::INLINE_MESSAGES_CLASSNAME . '-for-' . $element->getAttribute('name'));

            $newElement->setInlineErrorMessagesWrapper($wrapper);

            try {
                $element->parentNode->insertBefore($wrapper, $element->nextSibling);
            } catch(\Exception $e) {
                $element->parentNode->appendChild($wrapper);
            }


        }

    }

    public function getElements()
    {
        return $this->valliElements;
    }

    public function getFormDom()
    {
        return $this->formDom;
    }

    public function getElement(string $key)
    {
        return $this->valliElements[$key] ?? null;
    }

    public function addInput(string $type, string $name, string $value)
    {
        $input = $this->domDocument->createElement('input');
        $input->setAttribute('type', $type);
        $input->setAttribute('name', $name);
        $input->setAttribute('value', $value);
        $this->formDom->appendChild($input);
    }

    public function setRequestMethod(string $method)
    {
        $this->formDom->setAttribute('method', $method);
    }

    public function setAction(string $action)
    {
        $this->formDom->setAttribute('action', $action);
    }

    public function __toString()
    {
        return preg_replace("~.*<body>(.*)<\/body>.*~ms", "$1", $this->domDocument->saveHTML());
    }
    
}