<?php

namespace DanCharousek\VallI\Elements;

class VallIElementTextarea extends VallIElement implements IVallIElement
{

    public function setValue($value)
    {
        $this->domElement->nodeValue = $value;
    }

    public function getValue()
    {
        return $this->domElement->nodeValue;
    }

    public function __construct(\DOMDocument $document, \DOMElement $element)
    {
        parent::__construct($document, $element);
    }

}