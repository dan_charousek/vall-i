<?php

namespace DanCharousek\VallI\Elements;

interface IVallIElement
{

    function setValue($value);
    function getValue();

}