<?php

namespace DanCharousek\VallI\Elements;

class VallIElementSelectMultiple extends VallIElement implements IVallIElement
{

	private function getOptions()
	{
		return $this->domElement->getElementsByTagName('option');
	}

	private function getSelected()
	{
		$selected = [];
		foreach($this->getOptions as $option) {
			if($option->getAttribute('selected') == 'selected')
				$selected[] = $option;
		}
		return $selected;
	}

	public function setValue($values = [])
	{

		if(empty($values))
			return;

		foreach($this->getOptions() as $option) {

			$attrValue = $option->getAttribute('value');
			$optionsValue = ($attrValue == '' ? $option->firstChild->nodeValue : $attrValue);

			if(in_array($optionsValue, $values)) {
				$option->setAttribute('selected', 'selected');
			} else {
				$option->removeAttribute('selected');
			}
		}
	}

	public function getValue()
	{
		$values = [];

		foreach($this->getSelected() as $selected) {
			$value = $selected->getAttribute('value');
			$values[] = empty($value) ? $selected->firstChild->nodeValue : $value;
		}

		return $values;
	}

	public function __construct(\DOMDocument $document, \DOMElement $element)
	{
		parent::__construct($document, $element);
	}

}