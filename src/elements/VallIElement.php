<?php

namespace DanCharousek\VallI\Elements;

use DanCharousek\VallI\Rules\VallIRule;

class NoValliElementException extends \Exception {}

class VallIElement
{

	/**
	 * DOMDocument
	 * 
	 * @var DOMDocument
	 */
	protected $domDocument;

	/**
	 * DOMElement
	 * 
	 * @var DOMElement
	 */
	protected $domElement;

	/**
	 * DOMElement
	 * 
	 * @var DOMElement
	 */
	protected $inlineErrorMessagesWrapper;

	/**
	 * Mapping tagName/inputType to element class
	 * 
	 * @var array
	 */
	protected static $tagToInstance = [
		'text'              => VallIElementText::class,
		'password'          => VallIElementText::class,
		'number'            => VallIElementText::class,
		'email'				=> VallIElementText::class,
		'radio' 		    => VallIElementRadio::class,
		'checkbox'			=> VallIElementCheckbox::class,
		'textarea'			=> VallIElementTextarea::class,
		'select'			=> VallIElementSelect::class,
		'select-multiple'	=> VallIElementSelectMultiple::class,
		'file'      		=> VallIElementFile::class
	];

	/**
	 * List of rules attached to this element
	 * 
	 * @var array
	 */
	protected $valliRules = [];

	/**
	 * List of error messages attached to this element
	 * 
	 * @var array
	 */
	protected $errorMessages = [];

	/**
	 * Parses rules from valli-rules attribute
	 * 
	 * @return void
	 */
	protected function parseRules()
	{
		$plain = $this->domElement->getAttribute('valli-rules');
		$exploded = explode('|', $plain);
		foreach($exploded as $rule) {
			list($ruleName, $ruleValue) = explode(':', $rule);
			$rule = VallIRule::create($ruleName, $ruleValue, $this);
			$rule->setAttachedInputName($this->getName());
			$this->valliRules[$ruleName] = $rule;
		}
	}

	/**
	 * Add's inline error message after element
	 * 
	 * @param string $message
	 * @return void
	 */
	protected function addInlineErrorMessage($message)
	{
		$text = $this->domDocument->createTextNode($message);
		$span = $this->domDocument->createElement('span');
		$span->setAttribute('class', 'valli-inline-message');
		$span->appendChild($text);
		$this->inlineErrorMessagesWrapper->appendChild($span);
	}

	protected function removeValliAttributes()
	{
		$this->domElement->removeAttribute('valli-rules');
		$this->domElement->removeAttribute('valli-name');
	}

	public function __construct(\DOMDocument $document, \DOMElement $element)
	{
		$this->domDocument = $document;
		$this->domElement = $element;
		$this->parseRules();
		$this->removeValliAttributes();
	}
	
	/**
	 * Checks if element is valid agains given date
	 * 
	 * @param mixed $data
	 * @return void
	 */
	public function isValid($data): bool
	{
		$matches = true;
		foreach($this->valliRules as $rule) {
			if(!$rule->matches($data)) {
				$matches = false;
				$errorMessage = $rule->getErrorMessage();
				$this->errorMessages[] = $errorMessage;
				$this->addInlineErrorMessage($errorMessage);
			}
		}
		return $matches;
	}

	public function setInlineErrorMessagesWrapper(\DOMElement $wrapper)
	{
		$this->inlineErrorMessagesWrapper = $wrapper;
	}

	/**
	 * Returns element's name (value of attribute name of valli-name if specified)
	 * 
	 * @return void
	 */
	public function getName(): string
	{
		return $this->domElement->getAttribute('valli-name') ?? $this->domElement->getAttribute('name');
	}

	/**
	 * Returns list of error messages attached to this element
	 * 
	 * @return array
	 */
	public function getErrorMessages(): array
	{
		return $this->errorMessages;
	}

	/**
	 * Appends class to given element
	 * 
	 * @param string $class
	 * @return void
	 */
	public function setClass($class)
	{
		$current = $this->domElement->getAttribute('class');

		if(empty($current)) {
			$new = $class;
		} elseif(strpos($current, $class) == -1) {
			$new = $current . ' ' . $class;
		} else {
			$new = $current;
		}

		$this->domElement->setAttribute('class', $new);
	}
	
	public static function create(\DOMDocument $document, \DOMElement $element)
	{

		switch($element->tagName) {

			case 'input':
				$key = $element->getAttribute('type');
			break;

			case 'select':
				$multiple = $element->getAttribute('multiple');
				$key = empty($multiple) ? 'select' : 'select-multiple';
			break;

			default:
				$key = $element->tagName;
			break;

		}
		
		if(empty(VallIElement::$tagToInstance[$key]))
			throw new NoValliElementException('There is no such valli element with tag ' . $element->tagName);

		return new VallIElement::$tagToInstance[$key]($document, $element);
	}

}