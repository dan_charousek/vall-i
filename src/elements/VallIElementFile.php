<?php

namespace DanCharousek\VallI\Elements;

class VallIElementFile extends VallIElement implements IVallIElement
{

    public function setValue($value)
    {
    }

    public function getValue()
    {
        return $this->value;
    }

    public function __construct(\DOMDocument $document, \DOMElement $element)
    {
        parent::__construct($document, $element);
    }

}