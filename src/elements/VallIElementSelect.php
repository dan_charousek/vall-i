<?php

namespace DanCharousek\VallI\Elements;

class VallIElementSelect extends VallIElement implements IVallIElement
{

    private function getOptions()
    {
        return $this->domElement->getElementsByTagName('option');
    }

    private function getSelected()
    {
        foreach($this->getOptions as $option) {
            if($option->getAttribute('selected') == 'selected')
                return $option;
        }
        return null;
    }

    public function setValue($value)
    {

        if($value == '')
            return;

        foreach($this->getOptions() as $option) {

            $attrValue = $option->getAttribute('value');
            $optionsValue = ($attrValue == '' ? $option->firstChild->nodeValue : $attrValue);

            if($optionsValue == $value) {
                $option->setAttribute('selected', 'selected');
            } else {
                $option->removeAttribute('selected');
            }
        }
    }

    public function getValue()
    {
        $value = $this->getSelected()->getAttribute('value');
        return empty($value) ? $this->getSelected()->firstChild->nodeValue : $value;
    }

    public function __construct(\DOMDocument $document, \DOMElement $element)
    {
        parent::__construct($document, $element);
    }

}