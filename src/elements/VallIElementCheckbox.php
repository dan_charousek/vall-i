<?php

namespace DanCharousek\VallI\Elements;

class VallIElementCheckbox extends VallIElement implements IVallIElement
{

    public function setValue($value)
    {
        if((bool)$value) {
            $this->domElement->setAttribute('checked', 'checked');
        } else {
            $this->domElement->removeAttribute('checked');
        }
    }

    public function getValue()
    {
        return $this->domElement->getAttribute('checked') == 'checked';
    }

    public function __construct(\DOMDocument $document, \DOMElement $element)
    {
        parent::__construct($document, $element);
    }

}