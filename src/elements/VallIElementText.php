<?php

namespace DanCharousek\VallI\Elements;

class VallIElementText extends VallIElement implements IVallIElement
{

    public function setValue($value)
    {
        $this->domElement->setAttribute('value', $value);
    }

    public function getValue()
    {
        return $this->domElement->getAttribute('value');
    }

    public function __construct(\DOMDocument $document, \DOMElement $element)
    {
        parent::__construct($document, $element);
    }

}