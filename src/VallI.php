<?php

namespace DanCharousek\VallI;

use Nette\Http\RequestFactory;

/**
 * Exceptions used
 */
class SessionNotInitializedException extends \Exception {}
class FileNotFoundException extends \Exception {}
class NoFormSubmittedException extends \Exception {}

class VallI
{

    /**
     * Request methods
     */
    const REQUEST_METHOD_POST = 'post';
    const REQUEST_METHOD_GET = 'get';

    /**
     * VallI session keys
     */
    const SESSION_VALLI_KEY = 'valli';
    const SESSION_INVALID_FORM_KEY = 'invalid-form';
    const SESSION_SUBMITTED_FORM_KEY = 'submitted-form';

    const ID_INPUT_NAME = 'valli-form-id';

    /**
     * If true, inline error messages are shown under inputs
     * 
     * @var boolean
     */
    public $showInlineErrorMessages = true;

    /**
     * Keys of inputs that should not be restored in case form is invalid
     * 
     * @var array
     */
    private $notToRestore = [];

    /**
     * Classes added to elements after validation
     * 
     * @var array
     */
    private $inputClasses = ['valid' => 'VallIValidElement', 'invalid' => 'VallIInvalidElement'];

    /**
     * Form's ID - first 7 chars of file name's md5 hash
     * 
     * @var string
     */
    private $id;

    /**
     * Form
     * 
     * @var string
     */
    private $source;

    /**
     * Parameters passed to form template
     * 
     * @var array
     */
    private $templateParameters;

    /**
     * Form wrapper
     * 
     * @var VallIForm
     */
    private $formWrapper;

    /**
     * Loads form content from file and passes parameters
     * 
     * @param array $parameters
     * @return string
     */
    private static function getContentFromFile(string $path, array $parameters = []): string
    {
        foreach($parameters as $key => $value) {
            ${$key} = $value;
        }

        ob_start();
        include $path;
        $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }

    /**
     * Iterates over session stored form data and recovers it
     * 
     * @param array $data
     * @return void
     */
    private function recoverForm(VallIResult $result)
    {
        $this->formWrapper = new VallIForm($this->source);
        $result->formWrapper = $this->formWrapper;
        $result->notToRestore = $this->notToRestore;
        $result->inputClasses = $this->inputClasses;
        $result->validate();
        
        $_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_INVALID_FORM_KEY][$this->id] = null;
        unset($_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_INVALID_FORM_KEY][$this->id]);
    }

    /**
     * Init method
     * 
     * @return void
     */
    private function init()
    {

        /**
         * Checks if sesssion is started
         */
        if(!isset($_SESSION))
            throw new SessionNotInitializedException('VallI needs session to be initialized to work properly.');

        /**
         * If redirected from submitting form (isset SESSION_INVALID_FORM_KEY) recover data
         */
        if(!empty($_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_INVALID_FORM_KEY][$this->id]))
            $this->recoverForm(unserialize($_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_INVALID_FORM_KEY][$this->id]));
        
        $this->addHiddenIdInput();
    }

    /**
     * Adds hidden input with id
     * 
     * @return void
     */
    private function addHiddenIdInput()
    {
        $this->formWrapper->addInput('hidden', VallI::ID_INPUT_NAME, $this->id);
    }

    /**
     * Generates form's ID based on file's name
     * 
     * @param string $name
     * @return string
     */
    private static function createID(string $name)
    {
        return substr(md5($name), 0, 7);
    }

    public function __construct(string $source)
    {
        $this->formWrapper = new VallIForm($source);
    }

    /**
     * Set's form request method
     * 
     * @param string $method
     * @return void
     */
    public function setRequestMethod(string $method)
    {
        $this->formWrapper->setRequestMethod($method);
    }

    public function setNotToRestore(array $data)
    {
        $this->notToRestore = $data;
    }

    public function setInputClasses($valid, $invalid)
    {
        $this->inputClasses = [
            'valid' => $valid,
            'invalid' => $invalid
        ];
    }

    /**
     * Sets form submit destination
     * 
     * @param string $action
     * @return void
     */
    public function setAction(string $action)
    {
        $this->formWrapper->setAction($action);
    }

    /**
     * Returns form's ID
     * 
     * @return string
     */
    public function getID(): string
    {
        return $this->id;
    }

    public function save()
    {
        $this->init();
        $_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_SUBMITTED_FORM_KEY][$this->id] = serialize($this);
    }

    /**
     * Creates new VallI instance from form located in given file
     * 
     * @param string $path
     * @param string $extension
     * @return VallI
     */
    public static function createFromFile($name, $parameters = [], $extension = '.php')
    {
        $path = $name . $extension;

        if(!file_exists($path))
            throw new FileNotFoundException(sprintf('Form source file %s was not found', $path));

        $content = VallI::getContentFromFile($path, $parameters);
        $form = new VallI($content);
        $form->id = VallI::createID($name);
        $form->source = $content;
        $form->templateParameters = $parameters;

        return $form;
    }

    public static function validate()
    {
        $requestFactory = new RequestFactory();
        $request = $requestFactory->createHttpRequest();

        $data = [];

        if($request->getPost(VallI::ID_INPUT_NAME)) {
            $data = $request->getPost();
        } elseif ($request->getPost(VallI::ID_INPUT_NAME)) {
            $data = $request->getQuery();
        } else {
            throw new NoFormSubmittedException('No VallI form was submitted, can not validate.');
        }

        $formID = $data[VallI::ID_INPUT_NAME];

        if(empty($_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_SUBMITTED_FORM_KEY][$formID]))
            throw new NoFormKeyFoundException('There was no VallI form key found in session, didn\'t you clear?');

        $form = unserialize($_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_SUBMITTED_FORM_KEY][$formID]);
        $form->formWrapper = new VallIForm($form->source);

        $_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_SUBMITTED_FORM_KEY][$formID] = null;
        unset($_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_SUBMITTED_FORM_KEY][$formID]);

        $result = new VallIResult($form->formWrapper, $data);

        if(!$result->isValidFast()) {
            $_SESSION[VallI::SESSION_VALLI_KEY][VallI::SESSION_INVALID_FORM_KEY][$form->id] = serialize($result);
        }

        return $result;

    }

    public function __toString()
    {
        return (string)$this->formWrapper;
    }

}